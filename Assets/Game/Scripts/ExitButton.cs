﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitButton : MonoBehaviour
{
    [SerializeField] Select CrosswordSelect;
    GameObject parent;

    private void Start()
    { 
       
    }
    public void ExitOpen()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            if (gameObject.transform.GetChild(i).gameObject.activeSelf == true)
            {
                parent = gameObject.transform.GetChild(i).gameObject;
            }
        }
        if(GameObject.Find(parent.name))
        GameObject.Find(parent.name).SetActive(false);

        CrosswordSelect.hideObj = true;
        //GameObject gO = this.GetComponent<GameObject>();
        //Debug.Log(parent.gameObject.activeInHierarchy);
        //parent.SetActive(false);
        //Debug.Log(parent.gameObject.activeInHierarchy);
    }

    public void ClearAnswers()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            if (gameObject.transform.GetChild(i).gameObject.activeSelf == true)
            {
                parent = gameObject.transform.GetChild(i).gameObject;
            }
        }
        if (GameObject.Find(parent.name))
            GameObject.Find(parent.name).GetComponent<PuzzleCheck>().ClearAnswers();
    }

    
}
