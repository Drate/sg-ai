﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelection : MonoBehaviour
{
    private GameObject[] characterList;
    private int index;
    void Start()
    {
        index = PlayerPrefs.GetInt("CharacterSelected");

        characterList = new GameObject[transform.childCount];

        //fill array with models 
        for(int i = 0; i < transform.childCount;i++)
        {
            characterList[i] = transform.GetChild(i).gameObject;
        }
        // toggle off renderer
        foreach (GameObject go in characterList)
        {
            go.SetActive(false);
        }
        // toggle on
        if (characterList[index])
        {
            characterList[index].SetActive(true);
        }

    }

    public void ToggleLeft()
    {
        //toggle off current model
        characterList[index].SetActive(false);

        index--;
        if(index <0)
        {
            index = characterList.Length - 1;
        }

        //toggle on new model
        characterList[index].SetActive(true);
    }

    public void ToggleRight()
    {
        //toggle off current model
        characterList[index].SetActive(false);

        index++;
        if (index == characterList.Length )
        {
            index = 0;
        }

        //toggle on new model
        characterList[index].SetActive(true);
    }

    public void ConfirmButton()
    {
        PlayerPrefs.SetInt("CharacterSelected", index);
        SceneManager.LoadScene(2);
    }
}
