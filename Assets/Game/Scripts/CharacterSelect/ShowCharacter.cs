﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCharacter : MonoBehaviour
{
    private List<Transform> models;
    public Transform currentModel;
    private void Awake()
    {
        models = new List<Transform>();
        for (int i = 0; i < transform.childCount;i++)
        {
            var model = transform.GetChild(i);
            models.Add(model);

            model.gameObject.SetActive(i == 0);
        }
    }

    public void Enable(Transform modelTransform)
    {
        for (int i = 0; i <transform.childCount;i++)
        {
            var toToggle = transform.GetChild(i);
            bool shoudActive = toToggle == modelTransform;
            toToggle.gameObject.SetActive(shoudActive);

        }
        currentModel = modelTransform;
    }

    public List<Transform> GetModels()
    {
        return new List<Transform>(models);
    }
}
