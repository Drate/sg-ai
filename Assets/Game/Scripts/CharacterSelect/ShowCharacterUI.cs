﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCharacterUI : MonoBehaviour
{

    [SerializeField] private modelButton buttonPrafab;
    // Start is called before the first frame update
    void Start()
    {
        var models = FindObjectOfType<ShowCharacter>().GetModels();

        foreach(var model in models)
        {
            CreateButtonForModel(model);
        }
    }

    private void CreateButtonForModel(Transform model)
    {
        var button = Instantiate(buttonPrafab);
        button.transform.SetParent(this.transform);
        button.transform.localScale = Vector3.one;
        button.transform.localRotation = Quaternion.identity;

        var controller = FindObjectOfType<ShowCharacter>();
        button.Initialize(model, controller.Enable);

    }

}
