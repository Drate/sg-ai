﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SceneLoading : MonoBehaviour
{
    [SerializeField]
    private Image progressBar;
    // Start is called before the first frame update
    void Start()
    {
        //Ascnc operation
        StartCoroutine(LoadAsyncOperation(3));
    }

    IEnumerator LoadAsyncOperation(int sceneIndex)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneIndex); //Creating an async operation

        while (!asyncLoad.isDone)
        {
            //Debug.Log(Mathf.Clamp01(asyncLoad.progress / .9f));
            progressBar.fillAmount = Mathf.Clamp01(asyncLoad.progress/.9f); //Assigning progress as fillAmount of a progress bar
            yield return new WaitForEndOfFrame(); //To prevent crashing 
        }

    }
}
