﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : Interactable
{
    public override void Interact()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
    }

}
