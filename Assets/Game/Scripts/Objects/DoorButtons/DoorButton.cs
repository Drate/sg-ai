﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorButton : Interactable
{
    [SerializeField] GameObject[] doors;
    [SerializeField] GameObject[] taskObjects;
    AnimationSystem aS1 = null, aS2 = null;
    [SerializeField] int[] requiredScore;
    public override void Interact()
    {
        aS1 = doors[0].GetComponent<AnimationSystem>();
        aS2 = doors[1].GetComponent<AnimationSystem>();

        CheckForOpenClose();


       // if (aS1 != null && aS2 != null)
       // {
       //     aS1.OpenClose();
       //     aS2.OpenClose();
       // }
    }


    void CheckForOpenClose()
    {
        for (int i = 0; i < taskObjects.Length; i++)
        {
            taskObjects[i].GetComponent<CrosswordPuzzleStation>().GetScore();

            if (taskObjects[i].GetComponent<CrosswordPuzzleStation>().score >= requiredScore[i])
            {
                if (aS1 != null && aS2 != null)
                {
                    aS1.OpenClose();
                    aS2.OpenClose();
                }
            }
        }
    }
}

