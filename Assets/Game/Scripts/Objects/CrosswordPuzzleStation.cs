﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosswordPuzzleStation : Interactable
{
    public bool isPaused = false;
    //[SerializeField] GameObject CrosswordPanel;
    [SerializeField] Select Crossword;
    //int index; //OLD
    public bool[] GamesActive = {false,false };
    Shuffle shuffle;
    //Movement Player;
    GameObject selectedCrossword = null;
    public int score = 0;
    public override void Interact()
    {
        shuffle = gameObject.GetComponent<Shuffle>();
        //Player = GameObject.FindObjectOfType<Movement>();
        //Player.canMove = !Player.canMove;

        Debug.Log("Interacting with " + transform.name + "CrosswordPuzzleStation script " + hasInteracted);
        //index = 0; OLD

        //GamesActive[index] = true; //OLD
        //Crossword.SStart();
        // Crossword.hideObj = false; // OLD
        // Crossword.SelectObj(index, true); // OLD
        if(selectedCrossword !=null)
        {
            selectedCrossword.SetActive(true);

            
        }
        else
        {

            selectedCrossword = shuffle.SelectObjectWithTag();


            if(selectedCrossword != null)
            {
                if (selectedCrossword.GetComponent<Selected>().IsSelected != true)
                {
                    selectedCrossword.GetComponent<Selected>().IsSelected = true;
                }
                else
                {
                    selectedCrossword = null;
                }
            }
            


            if (selectedCrossword != null)
            {
                selectedCrossword.SetActive(true);
            }
            else
            {
                Debug.Log("There are no more crosswords");
            }
        }
        


    }
   

    public void GetScore()
    {
        if (selectedCrossword != null)
        {
            if (selectedCrossword.name.Contains("Questions"))
            {
                score = selectedCrossword.GetComponent<CompareText>().score *2;
            }
            else
            {
                score = selectedCrossword.GetComponent<CompareText>().score;
            }
            
            Debug.Log("Current Score " + score);
        }
        else
        {
            Debug.Log("No crossword assigned");
        }
    }

    

}
