﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;

public class PuzzleCheck : MonoBehaviour
{
    [SerializeField] TMP_Text[] Answer1 = null;
    [SerializeField] TMP_Text[] Answer2 = null;
    [SerializeField] TMP_Text[] Answer3 = null;
    [SerializeField] TMP_Text[] Answer4 = null;
    [SerializeField] TMP_Text[] Answer5 = null;
    [SerializeField] TMP_Text[] Answer6 = null;
    [SerializeField] TMP_Text[] Answer7 = null;
    [SerializeField] TMP_Text[] Answer8 = null;
    [SerializeField] TMP_Text[] Answer9 = null;
    [SerializeField] TMP_Text[] Answer10 = null;
    List<string> AndswerList = new List<string>();
    [SerializeField] CompareText CT;
    public void CheckPuzzle()
    {
        AddToList(Answer1);
        AddToList(Answer2);
        AddToList(Answer3);
        AddToList(Answer4);
        AddToList(Answer5);
        AddToList(Answer6);
        AddToList(Answer7);
        AddToList(Answer8);
        AddToList(Answer9);
        AddToList(Answer10);

       // foreach (string s in AndswerList)
       // {
       //
       //     Debug.Log(s + "<--- Answer");
       // }
        CT.Check(AndswerList, this.gameObject.name);

    }
    void AddToList(TMP_Text[] answer)
    {
        string s = "Not Answered";
        if (answer != null)
        {
            string c = "";
            foreach (TMP_Text a in answer)
            {

                c += a.text;
            }
            if (c != " ")
            {
                c = Regex.Replace(c, "[^\\w\\._]", "");
            }



            if (c == "")
            {
                c = s;
            }

            c = c.ToUpper();

            AndswerList.Add(c);
        }
        else
        {
            AndswerList.Add(s);
        }

    }

    public void ClearAnswers()
    {
        Clear(Answer1);
        Clear(Answer2);
        Clear(Answer3);
        Clear(Answer4);
        Clear(Answer5);
        Clear(Answer6);
        Clear(Answer7);
        Clear(Answer8);
        Clear(Answer9);
        Clear(Answer10);
        CT.Clear();
        AndswerList.Clear();
    }
    void Clear(TMP_Text[] answer)
    {
        for (int i = 0; i < answer.Length; i++)
        {
            var parent =answer[i].transform.parent.gameObject;
            var parent2 = parent.GetComponentInParent<TMP_InputField>();
            parent2.text = "";
            //Debug.Log("lol:" + parent2.text);

        }
    }
}
