﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class NextField : MonoBehaviour
{
    // Start is called before the first frame update
    public bool active;
    public Vector2 cordinates;
    public GameObject[] objectList = new GameObject[4]; // 0-Right 1-Down 2-Left 3-Up
    [SerializeField] GameObject CrosswordPanel;
    NextField[] fields;
    //private NextField obj;
    public bool selected = false;

    private void Start()
    {
        TMP_InputField t = transform.GetComponent<TMPro.TMP_InputField>();

        // UnityEngine.Events.UnityAction<string> call = new UnityAction<string>(Selected);
        //t.onSelect.AddListener(delegate{Selected();});
        t.onValueChanged.AddListener(delegate { OnValueChanged(); });
        t.onSelect.AddListener(delegate { Selected(); });
        t.onDeselect.AddListener(delegate { DeSelected(); });

        fields = GameObject.FindObjectsOfType<NextField>();

        FindClosest();
    }
    private void Update()
    {
        //FindClosest();

    }

    void FindClosest()
    {
        float distanceToClosestr = Mathf.Infinity;
        float distanceToClosestl = Mathf.NegativeInfinity;
        float distanceToClosestd = Mathf.NegativeInfinity;
        float distanceToClosestu = Mathf.Infinity;
        //NextField closest = null;


        foreach (NextField currentField in fields)
        {
            float rl = currentField.transform.position.x;
            float ud = currentField.transform.position.y;
           // float distanceToField = (currentField.transform.position - transform.position).sqrMagnitude;
           // if(distanceToField<distanceToClosest)
           // {
           //     distanceToClosest = distanceToField;
           //     closest = currentField;
           // }

            if((rl > transform.position.x && rl < transform.position.x + 50) && ud == transform.position.y)
            {
                if (rl < distanceToClosestr)
                {
                    distanceToClosestr = rl;
                   // Debug.Log(currentField.name + " R");
                    objectList[0] = currentField.gameObject;//Right
                }
            }
            if (rl < transform.position.x && rl > transform.position.x - 50 && ud == transform.position.y)
            {
                if (rl > distanceToClosestl)
                {
                    distanceToClosestl = rl;
                   // Debug.Log(currentField.name + " L");
                    objectList[2] = currentField.gameObject;//Left
                }
            }
            if (ud < transform.position.y && ud > transform.position.y - 50 && rl == transform.position.x)
            {
                if (ud > distanceToClosestd)
                {
                    distanceToClosestd = ud;
                   // Debug.Log(currentField.name + " D");
                    objectList[1] = currentField.gameObject;//Down
                }
            }
            if (ud > transform.position.y && ud < transform.position.y + 50 && rl == transform.position.x )
            {
                if (ud < distanceToClosestu)
                {
                    distanceToClosestu = ud;
                   // Debug.Log(currentField.name + " U");
                    objectList[3] = currentField.gameObject;//Up
                }
            }
        }

        //Debug.DrawLine(this.transform.position, closest.transform.position);
        
    }

    public void Selected()
    {
        selected = true;
    }
    public void DeSelected()
    {
        selected = false;
    }
    public void OnValueChanged()
    {
        //EventSystem.current.SetSelectedGameObject( objectList[0],null);
        UnityEngine.UI.Navigation Nav = this.GetComponent<TMP_InputField>().navigation;
        Nav.selectOnRight = objectList[0].GetComponent<TMP_InputField>();
        Nav.selectOnDown = objectList[1].GetComponent<TMP_InputField>();
        Nav.selectOnLeft = objectList[2].GetComponent<TMP_InputField>();
        Nav.selectOnUp = objectList[3].GetComponent<TMP_InputField>();
        this.GetComponent<TMP_InputField>().navigation = Nav;
        //this.GetComponent<TMP_InputField>().DeactivateInputField();
        //objectList[0].GetComponent<TMP_InputField>().Select();
        //objectList[0].GetComponent<TMP_InputField>().ActivateInputField();
        //if (this.GetComponent<TMP_InputField>().FindSelectableOnRight())
        if (objectList[0].GetComponent<TMP_InputField>().interactable != false)
        {
            //            if(objectList[2].GetComponent<TMP_InputField>().text=="" && objectList[2].GetComponent<TMP_InputField>().interactable != false  && objectList[1].GetComponent<TMP_InputField>().interactable != false)

            if ((objectList[2].GetComponent<TMP_InputField>().text=="" || objectList[2].GetComponent<TMP_InputField>().interactable == false)  && objectList[1].GetComponent<TMP_InputField>().interactable != false && objectList[1].GetComponent<TMP_InputField>().text == "" && objectList[3].GetComponent<TMP_InputField>().text != "")
            {
            this.GetComponent<TMP_InputField>().FindSelectableOnDown().Select();
               // Debug.Log("Down");
            }
            else
            {
                if(objectList[0].GetComponent<TMP_InputField>().text != "" && objectList[1].GetComponent<TMP_InputField>().interactable == true && objectList[3].GetComponent<TMP_InputField>().text != "")
                {
                    this.GetComponent<TMP_InputField>().FindSelectableOnDown().Select();
                   // Debug.Log("Down3");
                }
                else
                {
                    this.GetComponent<TMP_InputField>().FindSelectableOnRight().Select();
                   // Debug.Log("Right");
                }
            
            }
        }
        else
        {
            

            if(objectList[1].GetComponent<TMP_InputField>().interactable != false)
            {
                this.GetComponent<TMP_InputField>().FindSelectableOnDown().Select();
               // Debug.Log("Down2");
            }else
            {
                Debug.Log("Next input field is not selectable");
            }

        }
    }

}
