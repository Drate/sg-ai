﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selected : MonoBehaviour
{
    bool isSelected = false;

    public bool IsSelected { get => isSelected; set => isSelected = value; }
}
