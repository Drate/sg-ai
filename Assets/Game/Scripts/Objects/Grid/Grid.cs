﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grid: MonoBehaviour{
    int width, height;
    int[,] gridArray;
    float cellSize;
    [SerializeField] GameObject CrosswordPanel;
    [SerializeField] Sprite sprite;
    int Vertical, Horizontal,Rows,Columns;
    public void CreateGrid( int width, int height, float cellSize)
    {
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;

        RectTransform rt = CrosswordPanel.GetComponent<RectTransform>();

        Vertical = (int)Camera.main.orthographicSize;
        Horizontal = Vertical* ((int)rt.rect.width / (int)rt.rect.height);
        Rows = Vertical * 2;
        Columns = Horizontal * 2;

        gridArray = new int[height, width];

        //gridArray = new int[width, height];
        /*
        for (int x = 0; x < Columns; x++)
        {
            for (int y = 0; y < Rows; y++)
            {
                gridArray[x, y] = 0;
                spawnTile(x, y, gridArray[x, y]);
            }
        }*/
        Debug.Log("Vertical: " + Vertical + " Horizontal: " + Horizontal + " Rows: " + Rows + " Columns: " + Columns);
        
        for(int x =0; x < gridArray.GetLength(0); x++)
        {
            for (int y = 0; y < gridArray.GetLength(1); y++)
            {
                //CreateWorldText(CrosswordPanel.transform, gridArray[x, y].ToString(), GetWorldPosition(x, y) + new Vector3(cellSize, cellSize) * .5f, 50, Color.white, TextAnchor.MiddleCenter, TextAlignment.Center);

                spawnTile(GetWorldPosition(x, y) + new Vector3(cellSize, cellSize) * .5f);


            }
        }
    }
    private Vector3 GetWorldPosition(int x, int y)
    {
        return new Vector3(x, y) * cellSize;
    }

    
    private void spawnTile(Vector3 localPosition)
    {
        GameObject g = new GameObject("x: " + localPosition.x + " y: " + localPosition.y);
        g.transform.SetParent(CrosswordPanel.transform, true);
        g.transform.position =  new Vector3(localPosition.x - (Horizontal- (cellSize/2)), localPosition.y - (Vertical- (cellSize / 2)));
        
        g.layer = 5;
        var s = g.AddComponent<Image>();
        s.color = Color.white;
        s.sprite = sprite;
        g.GetComponent<RectTransform>().sizeDelta = new Vector2(cellSize, cellSize);
    }

    TextMesh CreateWorldText(Transform parent, string text, Vector3 localPosition, int fontSize, Color color, TextAnchor textAnchor, TextAlignment textAlignment)
    {
        GameObject gameObject = new GameObject("World_Text", typeof(TextMesh));
        Transform transform = gameObject.transform;
        
        transform.SetParent(parent, false);
        transform.localPosition = localPosition;
        TextMesh textMesh = gameObject.GetComponent<TextMesh>();
        textMesh.anchor = textAnchor;
        textMesh.alignment = textAlignment;
        textMesh.text = text;
        textMesh.fontSize = fontSize;
        textMesh.color = color;
        return textMesh;
    }
}
