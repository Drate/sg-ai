﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Select : MonoBehaviour
{
    [SerializeField] CrosswordPuzzleStation Station;
    private GameObject[] objectList;
    bool[] objectBool;
    [SerializeField] int id;

    public bool hideObj = false;
    private int index;
    void Start()
    {
        objectBool = new bool[transform.childCount];
        // index = PlayerPrefs.GetInt("CharacterSelected");

        objectList = new GameObject[transform.childCount];
        //bool[] objectBool2 = new bool[transform.childCount];
        //fill array with models 
        for (int i = 0; i < transform.childCount; i++)
        {
            objectList[i] = transform.GetChild(i).gameObject;
        }

        

        for (int i = 0; i < transform.childCount; i++)
        {
            //objectBool2[i] = true;
            objectBool[i] = true;
        }
       // objectBool = objectBool2;
        // toggle off renderer
        foreach (GameObject go in objectList)
        {
            go.SetActive(false);
        }
        // toggle on
        if (objectList[index] && Station.GamesActive[id])
        {
            objectList[index].SetActive(true);
        }

    }
    private void Update()
    {
        if(Station.GamesActive[id] && objectList[index])
       {
            if(hideObj == true)
            {
                objectList[index].SetActive(false);
            }
            else
            {

                objectList[index].SetActive(true);
            }
        }
    }

    public void SelectObj(int index, bool repetable)
    {
        if(index > transform.childCount)
        {
            index= Random.Range(0, transform.childCount);
        }
        if (repetable == false)
        {
            if(objectBool[index]== true)
            {
                SelectObjectInList(index, repetable);
            }
            else
            {
               index = Random.Range(0, objectList.Length +1);
               
                if(objectBool[index])
                {
                    SelectObjectInList(index, repetable);
                }else
                {
                    SelectObj(index, repetable);
                }

            }
            
        }
        else
        {
            SelectObjectInList(index, repetable);
        }


        
    }

    void SelectObjectInList(int index, bool repetable )
    {
        objectList[index].SetActive(false);
        if (repetable == false)
        {
            objectBool[index] = false;
        }


        if (index == objectList.Length)
        {
            index = 0;
        }

        if(objectBool[index])
        {
            objectList[index].SetActive(true);
        }

        

        PlayerPrefsX.SetBoolArray(transform.name, objectBool);
    }
}
