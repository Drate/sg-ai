﻿using UnityEngine;

public class Interactable : MonoBehaviour
{
    float radius = 3f;

    bool isFocus = false;

    Transform player;

    protected bool hasInteracted = false;

    public Transform interactionTransform;
    public virtual void Interact ()
    {
        Debug.Log("Interacting with " + transform.name);
    }

    private void Update()
    {
        if (isFocus && !hasInteracted)
        {
            float distance = Vector3.Distance(player.position, interactionTransform.position);
            if(distance <= radius)
            {
                hasInteracted = true;
                Interact();
                
            }
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(interactionTransform.position,radius);
    }

    public void OnFocused(Transform playerTransform)
    {
        isFocus = true;
        player = playerTransform;
        hasInteracted = false;
    }

    public void OnDefocused()
    {
        isFocus = false;
        player = null;
        hasInteracted = false;
    }
}
