﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CompareText : MonoBehaviour
{
    [SerializeField] List<TMP_Text> FinalAnswers;
    [SerializeField] List<Image> FinalCorrectness;
    [SerializeField] List<TMP_Text> FinalCorrectAnswers;
    [SerializeField] TMP_Text PointsFile;
    [SerializeField] Sprite[] sprites;
    [SerializeField] GameObject Resoult;
    [SerializeField] Points addPoints;
    bool[] correct;
    bool showScore = false;
    int endScore = 0, growthRate = 1, ammount = 10;
    public int score = 0;

    public void Check(List<string> LinesToCompare,string name)
    {
        

        string readFromFilePath = Application.streamingAssetsPath + "/Crossword/Answers/" + name + ".txt";
        string readFromFilePath2 = Application.streamingAssetsPath + "/Crossword/Answers/test.txt";
        List<string> fileLines = File.ReadAllLines(readFromFilePath).ToList();
        correct = new bool[fileLines.Count];
        for (int i =0; i < fileLines.Count; i++)
        {
            correct[i] = false;
        }
        

         for(int i =0; i < fileLines.Count; i++)
        {
            // string compare = LinesToCompare.ElementAt(i).ToString();
            //compare = Regex.Replace(compare, "[^\\w\\._]", "");
            //Debug.Log("Compare: "+ LinesToCompare.ElementAt(i).ToString());
            if (LinesToCompare.ElementAt(i).ToString() == fileLines.ElementAt(i).ToString().ToUpper())
            {
            
                correct[i] = true;
             //Debug.Log(correct[i]+": " + i);
            }else
            {
                correct[i] = false;
           // Debug.Log(correct[i] + ": " + i);
        }

            // foreach (bool c in correct)
            // {
            //     Debug.Log(c);
            // }
            
            //Debug.Log(fileLines.ElementAt(i).Length + " =" + fileLines.ElementAt(i).ToString());
            //Debug.Log(LinesToCompare.ElementAt(i).Length + " =" + LinesToCompare.ElementAt(i).ToString());
        }


        SetTheEndWindow(LinesToCompare, fileLines, correct);
    }
    private void Update()
    {
        if (PointsFile.IsActive() )
        {
            PointsFile.text = endScore.ToString();
        }

        if (showScore)
        {
            Resoult.SetActive(true);

            

            ShowScore();
           // showScore = false;
        }
    }

    void SetTheEndWindow(List<string> LinesToCompare, List<string> fileLines,bool[] correct)
    {

        for (int i = 0; i < fileLines.Count; i++)
        {
            
            FinalAnswers.ElementAt(i).text = LinesToCompare.ElementAt(i).ToString();

            if (correct[i])
            {
                FinalAnswers.ElementAt(i).color = Color.green;
            }
            else
            {
                //FinalAnswers.ElementAt(i).color = Color.red;
            }
        }

        for (int i = 0; i < fileLines.Count; i++)
        {
            
            FinalCorrectAnswers.ElementAt(i).text = fileLines.ElementAt(i).ToString();
            if(correct[i])
            {
                FinalCorrectAnswers.ElementAt(i).gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < fileLines.Count; i++)
        {
            FinalCorrectness.ElementAt(i).gameObject.SetActive(true);
            if (correct[i])
            {
                FinalCorrectness.ElementAt(i).GetComponent<Image>().sprite = sprites[0];

            }else
            {
                FinalCorrectness.ElementAt(i).GetComponent<Image>().sprite = sprites[1];
            }
        }

        foreach (bool c in correct)
        {
            if (c == true)
            {
                score += ammount;
            }
        }
        addPoints.AddPoints(score);
        showScore = true;
    }
    

    public void Clear()
    {
        addPoints.AddPoints(-score);
        score = 0;
        showScore = false;
    }


    void ShowScore()
    {
        if (endScore != score && score >= endScore)
        {
            endScore += growthRate;
        }
    }

}
