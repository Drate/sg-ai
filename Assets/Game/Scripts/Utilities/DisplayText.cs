﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class DisplayText : MonoBehaviour
{
    [SerializeField] GameObject Crossword;
    [SerializeField] TMP_InputField[] Question1 = null;
    [SerializeField] TMP_InputField[] Question2 = null;
    [SerializeField] TMP_InputField[] Question3 = null;
    [SerializeField] TMP_InputField[] Question4 = null;
    [SerializeField] TMP_InputField[] Question5 = null;
    [SerializeField] TMP_InputField[] Question6 = null;
    [SerializeField] TMP_InputField[] Question7 = null;
    [SerializeField] TMP_InputField[] Question8 = null;
    [SerializeField] TMP_InputField[] Question9 = null;
    [SerializeField] TMP_InputField[] Question10 = null;
    
    //Start is called before the first frame update
    void Start()
   {
    }

    // Update is called once per frame
    void Update()
    {
        LoadQuestion(Question1, 0);
        LoadQuestion(Question2, 1);
        LoadQuestion(Question3, 2);
        LoadQuestion(Question4, 3);
        LoadQuestion(Question5, 4);
        LoadQuestion(Question6, 5);
        LoadQuestion(Question7, 6);
        LoadQuestion(Question8, 7);
        LoadQuestion(Question9, 8);
        LoadQuestion(Question10, 9);
    }

   

    void LoadQuestion(TMP_InputField[] Question, int n)
    {
        string readFromFilePath = Application.streamingAssetsPath + "/Crossword/Questions/" + Crossword.name + ".txt";
        List<string> fileLines = File.ReadAllLines(readFromFilePath).ToList();
        // Debug.Log(Crossword.name);
        //Debug.Log(fileLines.ElementAt(n));
       // Debug.Log(Question == null);
        if (Question != null)
        {

            foreach (TMP_InputField q in Question)
            {
                
                if(q.GetComponent<NextField>().selected)
                {
                    //q.onSelect.AddListener(delegate { Test(fileLines.ElementAt(n).ToString()); });

                    //q.GetComponent<TMP_Text>().text = fileLines.ElementAt(n).ToString(); 

                    this.GetComponent<TMP_Text>().text = fileLines.ElementAt(n).ToString();
                }
            }
        }
    }

    


 //  void LoadQuestion2(TMP_InputField[] Question)
 //  {
 //      string readFromFilePath = Application.streamingAssetsPath + "/Crossword/Questions/" + Crossword.name + ".txt";
 //      List<string> fileLines = File.ReadAllLines(readFromFilePath).ToList();
 //      Debug.Log(fileLines.ElementAt(n).ToString());
 //      if (Question != null)
 //      {
 //
 //          foreach (TMP_InputField q in Question)
 //          {
 //
 //              q.onSelect.AddListener(delegate { Test(fileLines.ElementAt(n).ToString()); } );
 //               //   q.GetComponent<TMP_Text>().text = fileLines.ElementAt(n).ToString();
 //              
 //          }
 //
 //      }
 //  }
    
    void Test(string a)
    {

        this.GetComponent<TMP_Text>().text = a;
    }
}
