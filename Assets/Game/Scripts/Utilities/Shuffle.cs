﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class Shuffle : MonoBehaviour
{
    //public Text[] objectNames;
    GameObject[] AllGameObjects = null;
    //List<GameObject> AllGameObjects;
    GameObject chosenObject;
    GameObject[] chosenObjects;
    int index;
    System.Random random;
    [SerializeField] string ObjectTag;
    bool shuffle = false;
   //private void Update()
   //{
   //    if(ObjectTag != null)
   //    {
   //        AllGameObjects = GameObject.FindGameObjectsWithTag(ObjectTag);
   //    }
   //
   //}

    private void Start()
    {
        //var TableOfObjects = GameObject.FindObjectsOfType<CompareText>();
        List<GameObject> objectsInScene = new List<GameObject>();


        foreach (GameObject go in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
        {
            if (go.tag== ObjectTag)
                objectsInScene.Add(go);
        }
        AllGameObjects = objectsInScene.ToArray();
        //AllGameObjects = GameObject.FindObjectsOfTypeAll();
        //AllGameObjects = GameObject.FindGameObjectsWithTag(ObjectTag);
        //AllGameObjects = new GameObject[TableOfObjects.Length];
        //for (int i = 0; i < TableOfObjects.Length; i++)
        // {
        //     AllGameObjects[i] = TableOfObjects[i].gameObject;
        //     Debug.Log(" LOL" + AllGameObjects[0].name);
        // }
    }
   private void Update()
   {
       if (shuffle)
       {
            
            // AllGameObjects = GameObject.FindGameObjectsWithTag(ObjectTag);
            shuffle = false;
       }
       
   }
   
    void FindObjectsWithTag()
    {
        List<GameObject> objectsInScene = new List<GameObject>();


        foreach (GameObject go in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
        {
            if (go.tag == ObjectTag)
                objectsInScene.Add(go);
        }
        AllGameObjects = objectsInScene.ToArray();


        //AllGameObjects = new GameObject[TableOfObjects.Length];
        //Debug.Log(" LOL" + TableOfObjects[0].name);
        //for (int i = 0; i < TableOfObjects.Length;i++)
        //{
        //AllGameObjects[i] = TableOfObjects[i].gameObject;

        //}

        //AllGameObjects = GameObject.FindGameObjectsWithTag(Tag);


    }
    public GameObject SelectObjectWithTag()
    {
        //FindObjectsWithTag(Tag);
        //AllGameObjects = GameObject.FindGameObjectsWithTag(ObjectTag);
        //Debug.Log(AllGameObjects[0].name);
        if (AllGameObjects.Length>0)
        {
           var r = UnityEngine.Random.Range(0, AllGameObjects.Length);

            chosenObject = AllGameObjects[r];
            chosenObject.tag = "Chosen " + ObjectTag;
            Debug.Log(chosenObject.name);
            //objectNames[r].text = chosenObject.name;
            shuffle = true;
            FindObjectsWithTag();
            return chosenObject;
        }
        else
        {
            Debug.Log("NO");
            shuffle = true;
            FindObjectsWithTag();
            return null;
        }
    }
    public GameObject[] SelectObjectsWithTag(int Number)
    {
        //FindObjectsWithTag(Tag);
        //AllGameObjects = GameObject.FindGameObjectsWithTag(ObjectTag);
        if (AllGameObjects.Length >= Number)
        {
            for (int i = 0; i < Number; i++)
            {
                chosenObjects[i] = AllGameObjects[i];
                chosenObjects[i].tag = "Chosen " + ObjectTag;
                Debug.Log(chosenObjects[i].name);
               // objectNames[i].text = chosenObjects[i].name;
            }
            shuffle = true;
            FindObjectsWithTag();
            return chosenObjects;
        }
        else
        {
            Debug.Log("NO");
            shuffle = true;
            FindObjectsWithTag();
            return null;
        }
        
    }
}
