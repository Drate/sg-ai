﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;
public class VolumeControl : MonoBehaviour
{
    [SerializeField] string _volumeParameter = "MasterVolume";
    [SerializeField] TMP_Text _value;
    [SerializeField] AudioMixer _mixer;
    [SerializeField] Slider _slider;
    [SerializeField] float _multiplayer = 20f;

    private void Awake()
    {
        _slider.onValueChanged.AddListener(SliderValueChanged);
    }
    private void OnDisable()
    {
        PlayerPrefs.SetFloat(_volumeParameter, _slider.value);
    }

    private void SliderValueChanged(float value)
    {
        float newValue;
        if (value > 0.001)
        {
            newValue = Mathf.Log10(value) * _multiplayer;
        }else
        {
            newValue = -80f;
        }
        
        _mixer.SetFloat(_volumeParameter, newValue);
        _value.text = (value * 100).ToString();
    }

    // Start is called before the first frame update
    void Start()
    {
        _slider.value = PlayerPrefs.GetFloat(_volumeParameter, _slider.value);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
