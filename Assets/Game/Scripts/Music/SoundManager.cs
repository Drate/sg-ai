﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public static class SoundManager
{
    private static Dictionary<Sound, float> _soundTimeDictionary;

    private static GameObject _oneShotGO;
    private static AudioSource _oneShotAS;
    public enum Sound
    {
        PlayerMove,
        StoryIntroduction,
        Sound2,
        Sound3,
    }
   public static void PlaySound(Sound sound)
    {
        if (CanPlaySound(sound))
        {
            if(_oneShotGO ==null)
            {
                _oneShotGO = new GameObject("Sound");
                _oneShotAS = _oneShotGO.AddComponent<AudioSource>();

            }
            _oneShotAS.PlayOneShot(GetAudioClip(sound));
        }
    }

    private static AudioClip GetAudioClip(Sound sound)
    {
        foreach (GameAssets.AudioClipsClass audioClip in GameAssets.Instance.audioClipsClassArray)
        {
            if (audioClip.sound == sound)
            {
                return audioClip.audioClip;
            }
        }
        return null;
    }

    private static bool CanPlaySound(Sound sound)
    {
        switch (sound)
        {
            default:
                return true;

            case Sound.PlayerMove:
                if(_soundTimeDictionary.ContainsKey(sound))
                {
                    float lastTimePlayed = _soundTimeDictionary[sound];
                    float playerMoveTimer = 0.5f;

                    if(lastTimePlayed + playerMoveTimer < Time.time)
                    {
                        _soundTimeDictionary[sound] = Time.time;
                        return true;
                    }else
                    {
                        return false;
                    }
                }else
                {
                    return true;
                }

        }
    }

    public static void Initialize()
    {
        _soundTimeDictionary = new Dictionary<Sound, float>();
        _soundTimeDictionary[Sound.PlayerMove] = 0f;
    }
   
}
