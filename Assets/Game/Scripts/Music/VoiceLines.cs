﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class VoiceLines : MonoBehaviour
{
    private  Dictionary<Sound, int> _soundTimeDictionary;

    private static GameObject _oneShotGO;
    private static AudioSource _oneShotAS;
    
    bool[] alreadyPlayed;
    [SerializeField] AudioMixer _mixer;
    public enum Sound
    {
        StoryIntroduction,
        Sound2,
        Sound3,
    }
    public Sound playLineNumber =0;
    public float volume;
    public void PlaySound(Sound sound)
    {
        //if (CanPlaySound(sound))
       // {
            if (_oneShotGO == null)
            {
                _oneShotGO = new GameObject("Sound");
                _oneShotAS = _oneShotGO.AddComponent<AudioSource>();
                //_oneShotAS.outputAudioMixerGroup = _mixer.outputAudioMixerGroup;

            }
            _oneShotAS.PlayOneShot(GetAudioClip(sound));
        //}
    }
    
    private AudioClip GetAudioClip(Sound sound)
    {
        foreach (GameAssets.VoiceClipsClass audioClip in GameAssets.I.VoiceClipsClassArray)
        {
            if (audioClip.sound == sound)
            {
                return audioClip.audioClip;
            }
        }
        return null;
    }

    private bool CanPlaySound(Sound sound)
    {
        switch (sound)
        {
            default:
                return true;

            case Sound.StoryIntroduction:
                if (_soundTimeDictionary.ContainsKey(sound))
                {
                    float lastTimePlayed = _soundTimeDictionary[sound];
                    float playerMoveTimer = 1f;

                    if (lastTimePlayed + playerMoveTimer < Time.time)
                    {
                        //_soundTimeDictionary[sound] = Time.time;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }

        }
    }
    // Start is called before the first frame update
    void Start()
    {
        int i = 0;
        alreadyPlayed = new bool[GameAssets.I.VoiceClipsClassArray.Length];
        foreach (GameAssets.VoiceClipsClass audioClip in GameAssets.I.VoiceClipsClassArray)
        {
            alreadyPlayed[i] = false;
            i++;
        }
        playLineNumber = 0;
        _oneShotAS = GetComponent<AudioSource>();
        // _mixer.GetFloat("SFXVolume", out volume);
        //_oneShotAS.volume = volume;
    }

    // Update is called once per frame
    void Update()
    {
        int i = 0;
        foreach (GameAssets.VoiceClipsClass audioClip in GameAssets.I.VoiceClipsClassArray)
        {
            if (alreadyPlayed[i] == false)
                {
                    PlaySound(playLineNumber);
                    alreadyPlayed[i] = true;

                }
            i++;
        }

       // if (_oneShotGO != null)
        //{
         //   bool result =_mixer.GetFloat("SFXVolume",out volume);
        //    Debug.Log(volume+10);
        //    _oneShotAS.volume = Mathf.Clamp(volume+10, 0.1f, 1f);
       // }
        //_mixer.GetFloat("SFXVolume",out volume);
        //Debug.Log(volume);
        //_oneShotAS.volume = volume;

    }
}
