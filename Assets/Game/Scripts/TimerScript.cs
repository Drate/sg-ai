﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimerScript : MonoBehaviour
{
    [SerializeField] TMP_Text timerTextField;
    [SerializeField] GameObject breakWindow;
    float globalTime;
    int breakTime =40;
    // Start is called before the first frame update
    void Start()
    {
        globalTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        //+ 2370
        float t = Time.time - globalTime;
        string hours = ((int)t / 60 / 60).ToString();
        string minutes = ((int)t / 60 % 60).ToString();
        string seconds = ((int)t % 60).ToString();

        if((int)t / 60 / 60 < 10)
        {
            hours = "0" + hours;
        }
        if ((int)t / 60 < 10)
        {
            minutes = "0" + minutes;
        }
        if ((int)t % 60< 10)
        {
            seconds = "0" + seconds;
        }


        timerTextField.text = hours + ":" + minutes + ":" + seconds;

        if ((int)t / 60 % 60 >= breakTime)
        {
            BreakTime();
        }


    }

    void BreakTime()
    {
        breakTime += 40;
        breakWindow.GetComponent<AnimationSystem>().ShowHide(5);



    }

}
