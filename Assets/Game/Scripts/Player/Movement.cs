﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    CharacterController controller;
    [SerializeField]
    Transform groundCheck;
    [SerializeField]
    LayerMask groundMask;
    [SerializeField]
    VoiceLines voiceLines;

    Vector3 velocity;
    float speed = 24f;
    float gravity = -9.81f;
    float groundDistance = 0.4f;
    float jumpHeight = 3;
    bool isGrounded;
    float gravityMultiplayer = 3;
    public bool canMove = true;
    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(GameObject.FindObjectOfType<PuzzleCheck>() != null)
        {
            if (GameObject.FindObjectOfType<PuzzleCheck>().gameObject.activeSelf)
            {
                canMove = false;
            }
            else
            {
                canMove = true;
            }
        }
        else
        {
            canMove = true;
        }


        if (isGrounded && velocity.y <0)
        {
            velocity.y = -2f;
        }

        if(canMove)
        {
            playerMovement();

            jump();
        }
        

        Gravity();
    }
    
    void jump() // v = sqrt(h * -2 * g)
    {
        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);

            //voiceLines.playLineNumber = 0;
        }

    }
    void playerMovement()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
        bool isIdle = x == 0 && z == 0;

        if (!isIdle) SoundManager.PlaySound(SoundManager.Sound.PlayerMove);
        

        controller.Move(move * speed * Time.deltaTime);
    }

    void Gravity()//y = 1/2g * t^2
    {
        velocity.y += gravity * gravityMultiplayer * Time.deltaTime ;
        controller.Move(velocity * Time.deltaTime);
    }
}
