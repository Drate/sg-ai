﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class userParameters : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform Model;
    static public Transform playerModel;

    void Start()
    {
        UpdateModel(Model);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateModel(Transform model)
    {
      playerModel = model;
        Enable(playerModel);
    }

    public void Enable(Transform modelTransform)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var toToggle = transform.GetChild(i);
            bool shoudActive = toToggle == modelTransform;
            toToggle.gameObject.SetActive(shoudActive);

        }
    }
}
