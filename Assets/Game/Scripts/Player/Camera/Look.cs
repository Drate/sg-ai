﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Look : MonoBehaviour
{
    public Interactable focus;
    // Start is called before the first frame update
    float mouseSensitivity = 250f;

    public Transform Body;

    float xRotation = 0f;

    Camera cam;
    bool mouseRelease = false;

    Points p;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        cam = Camera.main;
        p = GameObject.FindObjectOfType<Points>();
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        
        //Coursor Lock/Release controll
        if(Input.GetKeyDown(KeyCode.LeftAlt))
        {
            mouseRelease = !mouseRelease;
            
        }

       //if (Input.GetKeyDown(KeyCode.R))
      //{
           
       //    p.AddPoints(10);
       //}


        if (mouseRelease == true)
        {
            if (Cursor.lockState != CursorLockMode.Confined)
            {
                Cursor.lockState = CursorLockMode.Confined;
            }


        }else if(UIMenu.isPaused == false)
        {

            if(Cursor.lockState != CursorLockMode.Locked)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, 0f, 60f);

            transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
            Body.Rotate(Vector3.up * mouseX);
        }



        if (Input.GetMouseButtonDown(0))
        {
            //Create a ray
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            //if ray hits something
            if (Physics.Raycast(ray, out hit, 100))
            {
               
               
             RemoveFocus();
               
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            //Create a ray
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            //if ray hits something
            if(Physics.Raycast(ray,out hit, 100))
            {
                Interactable interaction = hit.collider.GetComponent<Interactable>();

                if(interaction !=null)
                {
                    SetFocus(interaction);
                }
            }
        }

    }

    void SetFocus(Interactable newFocus)
    {
        if (newFocus != focus)
        {
            if (focus != null)
            focus.OnDefocused();

            focus = newFocus;
            
        }

        newFocus.OnFocused(transform);
    }
    void RemoveFocus()
    {
        if (focus != null)
        focus.OnDefocused();

        focus = null;
    }
}
