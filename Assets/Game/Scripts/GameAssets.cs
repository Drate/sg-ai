﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GameAssets : MonoBehaviour
{
    private static GameAssets _instance;
    public AudioClipsClass[] audioClipsClassArray;
    public VoiceClipsClass[] VoiceClipsClassArray;
    public static GameAssets Instance
    { get
        {
            if (_instance == null)
            {
                _instance = (Instantiate(Resources.Load("GameAssets")) as GameObject).GetComponent<GameAssets>();
            }
            return _instance;
        }
    }
    public static GameAssets I
    {
        get
        {
            if (_instance == null)
            {
                _instance = (Instantiate(Resources.Load("GameAssets2")) as GameObject).GetComponent<GameAssets>();
            }
            return _instance;
        }
    }

    [System.Serializable]
    public  class AudioClipsClass {
        public SoundManager.Sound sound;
        public AudioClip audioClip;

    }
    [System.Serializable]
    public class VoiceClipsClass
    {
        public VoiceLines.Sound sound;
        public AudioClip audioClip;

    }
    
}
