﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Points : MonoBehaviour
{
    [SerializeField] TMP_Text pointCounter;
    
    string sPoints = "";
    int endScore = 0, points = 0, growthRate = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public int GetPoints()
    {
        return points;
    }
    // Update is called once per frame
    void Update()
    {
        

        sPoints = "Points: "+ endScore.ToString();
        pointCounter.text = sPoints;
  
            UpdatePoints();
    }
    
   

    void UpdatePoints()
    {
        if (endScore != points && points >= endScore)
        {
            endScore += growthRate;
        }
    }

    public void AddPoints(int ammount)
    {
        points += ammount;
        if (ammount <= 0)
        {
            endScore += ammount;
        }
            
        
    }
}
