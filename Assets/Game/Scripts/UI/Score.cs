﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    private TMP_Text text;
    private int score;
    
    public static Score Instance { get; private set; }

    private void Awake()
    {
        text = GetComponent<TMP_Text>();
        Instance = this;
    }
    public void AddPoints(int value)
    {
        score += value;
        text.SetText(score.ToString());
    }
}
