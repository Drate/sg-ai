﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class SetTextFromButton : MonoBehaviour
{
    [SerializeField] TMP_InputField inputField;
    public void ButtonSetText()
    {
            
        //inputField.textComponent.SetText(this.GetComponent<TMP_Text>().text);
        string q = GetComponentInChildren<TMP_Text>().text;
        inputField.text = q;

    }
}
