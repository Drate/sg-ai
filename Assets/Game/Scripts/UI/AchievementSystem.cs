﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AchievementSystem : MonoBehaviour
{
    int RegisteredPoints;
    Points points;
    IDictionary<int, string> numberNames;
    bool[] alreadyGained;
    AnimationSystem aS;
    [SerializeField] TMP_Text achivementText;
    void Start()
    {
        points = GameObject.FindObjectOfType<Points>();
        numberNames = new Dictionary<int, string>();
        numberNames.Add(50, "The beginnings can be tough, but you can handle it!");
        numberNames.Add(100, "That's one small step for man, but at least we are one step closer to the rest of mankind...or something like that");
        numberNames.Add(150, "So you are not a beginner, huh?");
        numberNames.Add(200, "(Ex)Terminator of questions");
        numberNames.Add(250, "On the way home");

        alreadyGained = new bool[numberNames.Count];
        //Debug.Log(alreadyGained.Length);
        //Debug.Log(numberNames.Count);
        for (int i =0;i< numberNames.Count;i++)
        {
            alreadyGained[i] = false;
        }
        aS = this.GetComponent<AnimationSystem>();

    }

    // Update is called once per frame
    void Update()
    {
        RegisteredPoints=points.GetPoints();
        //Debug.Log(RegisteredPoints);
        int i = 0;
        foreach (KeyValuePair<int, string> kvp in numberNames)
        {

            
            if (kvp.Key <= RegisteredPoints)
            {
                if (alreadyGained[i] ==false)
                {
                    achivementText.text = kvp.Value;
                    aS.ShowHide(5);
                    alreadyGained[i] = true;
                    
                }

            }
            i++;
        }


        

    }
    


}
