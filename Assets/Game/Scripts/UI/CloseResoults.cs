﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseResoults : MonoBehaviour
{
    public void Close()
    {
        CloseResoults[] Windows = GameObject.FindObjectsOfType<CloseResoults>();

        foreach(CloseResoults cr in Windows)
        {
            if(cr.gameObject.activeSelf)
            {
                cr.gameObject.SetActive(false);
            }
        }
        
    }


}
