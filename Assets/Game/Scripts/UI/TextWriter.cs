﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextWriter : MonoBehaviour
{
    // Start is called before the first frame update
    Text uiText;
    string textToWrite;
    float speed;
    float timer;
    int characterIndex;
    public void Write(Text uiText,string textToWrite, float speed)
    {
        this.uiText = uiText;
        this.textToWrite = textToWrite;
        this.speed = speed;
        characterIndex = 0;
    }

    private void Update()
    {
        if(uiText != null)
        {
            timer -= Time.deltaTime;
            if(timer <= 0f)
            {
                timer += speed;
                characterIndex++;
                uiText.text = textToWrite.Substring(0,characterIndex);

                if (characterIndex >= textToWrite.Length)
                {
                    uiText = null;
                    return;
                }
            }
        }
    }

}
