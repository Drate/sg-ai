﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenu : MonoBehaviour
{
    public static bool isPaused = false;

    [SerializeField] GameObject optionsWindow;
    [SerializeField] GameObject Crossfire;
    public GameObject MenuUI;

    bool hide = false;
    // Update is called once per frame
    void Update()
    {
        if(MenuUI.activeSelf && isPaused == false)
        {
            MenuUI.SetActive(false);
        }
        
    }

    public void OnClickQuit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
    }

    public void PauseButton()
    {
        if (isPaused)
        {

            Resume();
            Crossfire.SetActive(true);
        }
        else
        {
            
            Pause();
            Crossfire.SetActive(false);
        }
    }

    void Resume()
    {
        MenuUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }

    void Pause()
    {
        MenuUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }
}
