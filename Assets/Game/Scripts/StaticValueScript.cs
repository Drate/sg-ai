﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticValueScript : MonoBehaviour
{
    static private Transform selectedCharacter;
    static public Transform SelectedCharacter { get { return selectedCharacter; } set { selectedCharacter = value; } }
}
