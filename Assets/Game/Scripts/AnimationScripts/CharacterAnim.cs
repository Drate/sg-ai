﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterAnim : MonoBehaviour
{
    // Start is called before the first frame update
    private Animator anim;
    void Start()
    {
        anim = GetComponent<Animator>();
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "CharacterSelect")
        {
            anim.SetBool("isCharacterSelection", true);
        }
        else
        {
            anim.SetBool("isCharacterSelection", false);
        }
        if (SceneManager.GetActiveScene().name == "Level 1")
        {
            anim.SetBool("isLevel", true);
        }
        else
        {
            anim.SetBool("isLevel", false);
        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {

        }
    }
}
