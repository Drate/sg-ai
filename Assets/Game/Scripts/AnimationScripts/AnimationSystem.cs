﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationSystem : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Sprite[] sprites = null;
    [SerializeField] Button button = null;
    public bool hide = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (hide)
        {
            Animator animator = this.GetComponent<Animator>();
            bool isActive = animator.GetBool("active");
            animator.SetBool("active", !isActive);
            hide = false;
        }
    }
    IEnumerator Wait(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        hide = true;
    }

    public void ShowHide(int closeAfterTime =3)
    {
        if(closeAfterTime ==0)
        {
            closeAfterTime = 3;
        }
        if (closeAfterTime == -1)
        {
            closeAfterTime = 3;
        }
        if (this.gameObject != null)
        {
            Animator animator = this.GetComponent<Animator>();
            if (animator != null)
            {
                bool isActive = animator.GetBool("active");

                animator.SetBool("active", !isActive);

                if (button != null)
                {
                    if (sprites != null)
                    {
                        if (animator.GetBool("active") == true)
                        {
                            button.GetComponent<Image>().sprite = sprites[1];
                        }
                        else
                        {
                            button.GetComponent<Image>().sprite = sprites[0];
                        }
                    }
                    else
                    {
                        Debug.Log("Animatons are missing");
                    }

                }
                
                StartCoroutine(Wait(closeAfterTime));

            }else
            {
                Debug.Log("Animaton and/or animation controller file is missing");
            }

        }

    }
    public void OpenClose()
    {
        if (this.gameObject != null)
        {
            Animator animator = this.GetComponent<Animator>();
            if (animator != null)
            {
                bool isActive = animator.GetBool("active");

                animator.SetBool("active", !isActive);
                
            }
            else
            {
                Debug.Log("Animaton and/or animation controller file is missing");
            }

        }

    }
}
