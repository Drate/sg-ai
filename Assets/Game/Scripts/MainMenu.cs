﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour
{
    [SerializeField] GameObject settingsWindow;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(3.0f);
        

    }
    public void OnClickStart()
    {
        SceneManager.LoadScene(1);
    }
    public void OnClickQuit()
    {
    #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
    #else
		Application.Quit();
    #endif
    }

    public void SettingsButton()
    {
        if (settingsWindow != null)
        {
            Animator animator = settingsWindow.GetComponent<Animator>();
            if (animator != null)
            {
                bool isActive = animator.GetBool("active");

                animator.SetBool("active", !isActive);
                
            }

        }

    }
}
